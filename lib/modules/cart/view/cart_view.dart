import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_flutter_architecture/common/app_colors.dart';
import 'package:riverpod_flutter_architecture/data/models/product_model.dart';
import 'package:riverpod_flutter_architecture/modules/cart/providers/cart_provider.dart';

class CartPage extends ConsumerWidget {
  const CartPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final cartController = ref.watch(cartControllerProvider);

    return Scaffold(
      backgroundColor: AppColors.offWhite,
      appBar: AppBar(
          title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: const [
          Text('Cart'),
        ],
      )),
      body: SafeArea(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Expanded(
              child: ListView.builder(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                physics: const AlwaysScrollableScrollPhysics(),
                itemCount: cartController.cartProductList.length,
                itemBuilder: (context, index) {
                  return getProductCard(
                      cartController.cartProductList[index], ref);
                },
              ),
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }

  Widget getProductCard(Product product, WidgetRef ref) {
    return Card(
      elevation: 1,
      color: AppColors.FFCED0FF,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 20),
        child: Row(children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(product.productName ?? ""),
              Text("Quantity: ${product.quantity}"),
            ],
          )),
          InkWell(
            onTap: () {
              ref.read(cartControllerProvider.notifier).removeProduct(product);
            },
            child: Icon(
              Icons.delete,
              color: AppColors.errorColor.withOpacity(0.7),
            ),
          )
        ]),
      ),
    );
  }
}
