import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_flutter_architecture/modules/cart/view%20model/cart_view_model.dart';

final cartControllerProvider =
    ChangeNotifierProvider((ref) => CartController());
