import 'package:flutter/material.dart';
import 'package:riverpod_flutter_architecture/data/models/product_model.dart';

class CartController extends ChangeNotifier {
  CartController() : super();

  List<Product> cartProductList = List.empty(growable: true);

  void addProduct(Product product) {
    cartProductList.add(product);
    notifyListeners();
  }

  void removeProduct(Product product) {
    cartProductList.remove(product);
    notifyListeners();
  }
}
