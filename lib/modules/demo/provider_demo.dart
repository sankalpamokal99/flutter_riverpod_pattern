import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final stringProvider = Provider.family<String, String>((ref, id) {
  return id;
});

class ProviderDemo extends ConsumerWidget {
  const ProviderDemo({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
        appBar: AppBar(title: const Text("Provider Demo")),
        body: Center(child: Text(ref.read(stringProvider('Hello')))));
  }
}

//ProviderScope

// WidgetRef as an object that allows widgets to interact with providers.

// Consumer, ConsumerWidget

//watch, read, listen

//You may use this to access a repository, a logger, or some other class that doesn't contain mutable state.

// Provider is great for accessing dependencies that don't change, such as the repositories in our app

//family is a modifier that we can use to pass an argument to a provider.

//autoDispose - the provider will be disposed once all listeners are removed (that is, when the widgets are unmounted).
