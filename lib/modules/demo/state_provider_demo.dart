import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final counterStateProvider = StateProvider<int>((ref) {
  return 0;
});

class StateProviderDemo extends ConsumerWidget {
  const StateProviderDemo({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ref.listen(counterStateProvider, (previous, next) {
      if (next == 5)
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Value is ${next}')),
        );
    });
    return Scaffold(
      appBar: AppBar(title: const Text("State Provider Demo")),
      body: Center(
          child: Text(
        ref.watch(counterStateProvider).toString(),
        style: const TextStyle(fontSize: 40),
      )),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          FloatingActionButton(
              onPressed: () {
                ref.read(counterStateProvider.notifier).state++;
              },
              child: const Icon(Icons.add)),
          FloatingActionButton(
              onPressed: () {
                ref
                    .read(counterStateProvider.notifier)
                    .update((state) => state - 1);
              },
              child: const Icon(Icons.minimize_outlined)),
        ],
      ),
    );
  }
}

//StateProvider is great for storing simple state objects that can change

// StateProvider is ideal for storing simple state variables, such as enums, strings, booleans, and numbers

//update method

//   ref.listen(counterStateProvider, (previous, next) {
//       if (next == 5)
//         ScaffoldMessenger.of(context).showSnackBar(
//           SnackBar(content: Text('Value is ${next}')),
//         );
//     });
