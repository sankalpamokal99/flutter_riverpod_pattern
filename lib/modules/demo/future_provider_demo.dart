import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class FutureProviderDemo extends ConsumerWidget {
  const FutureProviderDemo({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    AsyncValue<Weather> weather = ref.watch(weatherFutureProvider);

    return Scaffold(
      body: Center(
        child: weather.when(
            data: (weather) => Text(
                  "${weather.city} - ${weather.atmosphere}",
                  style: const TextStyle(fontSize: 40),
                ),
            error: (err, stack) => Text(
                  err.toString(),
                  style: const TextStyle(fontSize: 40, color: Colors.red),
                ),
            loading: () => const CircularProgressIndicator()),
      ),
    );
  }
}

final weatherFutureProvider = FutureProvider.autoDispose<Weather>((ref) {
  final weatherRepository = ref.read(weatherRepositoryProvider);
  return weatherRepository.getWeather(city: 'Navi Mumbai');
});

final weatherRepositoryProvider = Provider<WeatherRepository>((ref) {
  return WeatherRepository(); // declared elsewhere
});

class WeatherRepository {
  Future<Weather> getWeather({required String city}) async {
    await Future.delayed(const Duration(seconds: 5));
    Weather data = Weather.fromJson({'atmosphere': 'Rainy', 'city': city});
    return data;
  }

  Stream<int> getStreamData() async* {
    int i = 0;
    while (true) {
      await Future.delayed(const Duration(seconds: 1));
      yield i++;
    }
  }
}

class Weather {
  String? city;
  String? atmosphere;
  Weather({this.city, this.atmosphere});

  factory Weather.fromJson(Map<String, dynamic> json) => Weather(
        city: json["city"],
        atmosphere: json["atmosphere"],
      );
}

// Caching with Timeout - ref.keepAlive();

// get the [KeepAliveLink]
// final link = ref.keepAlive();
// start a 30 second timer
// final timer = Timer(const Duration(seconds: 30), () {
// dispose on timeout
//   link.close();
// });

// ref.onDispose(() => timer.cancel());

// In total, there are five different callbacks that we can use:

// ref.onDispose: triggered right before the provider is destroyed
// ref.onCancel: triggered when the last listener of the provider is removed
// ref.onResume: triggered when a provider is listened again after it was paused
// ref.onAddListener: triggered whenever a new listener is added to the provider
// ref.onRemoveListener: triggered whenever a new listener is removed from the provider
