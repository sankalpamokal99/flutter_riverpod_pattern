import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';

class StateNotifierProviderDemo extends ConsumerWidget {
  const StateNotifierProviderDemo({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final DateTime currentTime = ref.watch(clockProvider);

    return Scaffold(
      appBar: AppBar(title: const Text("State Provider Demo")),
      body: Center(
          child: Text(
        DateFormat.Hms().format(currentTime),
        style: const TextStyle(fontSize: 40),
      )),
    );
  }
}

class Clock extends StateNotifier<DateTime> {
  late final Timer _timer;

  Clock() : super(DateTime(1947, 1, 1, 1, 1, 1)) {
    _timer = Timer.periodic(const Duration(seconds: 1), (_) {
      state = DateTime.now();
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
}

final clockProvider = StateNotifierProvider<Clock, DateTime>((ref) {
  return Clock();
});

//StateNotifierProvider and StateNotifier are ideal for managing state that may change in reaction to an event or user interaction.

//updates ui whenever state changes

//Initialize in super constructor
