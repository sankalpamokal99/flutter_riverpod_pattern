import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_flutter_architecture/common/app_colors.dart';
import 'package:riverpod_flutter_architecture/data/models/product_model.dart';
import 'package:riverpod_flutter_architecture/modules/cart/providers/cart_provider.dart';
import 'package:riverpod_flutter_architecture/modules/cart/view/cart_view.dart';
import 'package:riverpod_flutter_architecture/modules/products/providers/products_provider.dart';

class ProductsPage extends ConsumerWidget {
  const ProductsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final productsController = ref.watch(productControllerProvider);
    final cartController = ref.watch(cartControllerProvider);

    return Scaffold(
      backgroundColor: AppColors.offWhite,
      appBar: AppBar(
          title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text('Products'),
          InkWell(
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => const CartPage()));
            },
            child: Badge(
              isLabelVisible: true,
              label: Text('${cartController.cartProductList.length}' ?? ''),
              child: const Icon(Icons.shopping_cart),
            ),
          )
        ],
      )),
      body: SafeArea(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Expanded(
              child: ListView.builder(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                physics: const AlwaysScrollableScrollPhysics(),
                itemCount: productsController.productsList.length,
                itemBuilder: (context, index) {
                  return getProductCard(
                      productsController.productsList[index], ref);
                },
              ),
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }

  Widget getProductCard(Product product, WidgetRef ref) {
    return Card(
      elevation: 1,
      color: AppColors.FFCED0FF,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 20),
        child: Row(children: [
          Expanded(child: Text(product.productName ?? "")),
          InkWell(
            child: const Icon(Icons.add_shopping_cart),
            onTap: () {
              ref.read(cartControllerProvider.notifier).addProduct(product);
            },
          )
        ]),
      ),
    );
  }
}
