import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_flutter_architecture/modules/products/view%20model/products_view_model.dart';

// final productsViewProvider =
//     StateNotifierProvider<ProductsViewModel, List<Product>>(
//         (ref) => ProductsViewModel());

final productControllerProvider =
    ChangeNotifierProvider((ref) => ProductsController());
