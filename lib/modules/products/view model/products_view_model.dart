import 'package:flutter/cupertino.dart';
import 'package:riverpod_flutter_architecture/data/models/product_model.dart';

class ProductsController extends ChangeNotifier {
  ProductsController() : super() {
    debugPrint("constructor called");
    getProductsList();
  }

  List<Product> productsList = List.empty(growable: true);

  Future<bool> addProduct() async {
    //API CALL
    return true;
  }

  Future<void> getProductsList() async {
    debugPrint("method called");

    //API CALL
    productsList.add(Product(
        image: '',
        productName: 'Apple',
        quantity: 0,
        productDesc: 'Apple it is'));
    productsList.add(Product(
        image: '',
        productName: 'Banana',
        quantity: 0,
        productDesc: 'Banana it is'));
    productsList.add(Product(
        image: '',
        productName: 'Orange',
        quantity: 0,
        productDesc: 'Orange it is'));
    productsList.add(Product(
        image: '',
        productName: 'Lemon',
        quantity: 0,
        productDesc: 'Lemon it is'));
    productsList.add(Product(
        image: '',
        productName: 'Cherry',
        quantity: 0,
        productDesc: 'Cherry it is'));
    productsList.add(Product(
        image: '',
        productName: 'Guava',
        quantity: 0,
        productDesc: 'Guava it is'));
    productsList.add(Product(
        image: '',
        productName: 'Peach',
        quantity: 0,
        productDesc: 'Peach it is'));
    productsList.add(Product(
        image: '',
        productName: 'Watermelon',
        quantity: 0,
        productDesc: 'Watermelon it is'));
    productsList.add(Product(
        image: '',
        productName: 'Pineapple',
        quantity: 0,
        productDesc: 'Pineapple it is'));
    productsList.add(Product(
        image: '',
        productName: 'Mango',
        quantity: 0,
        productDesc: 'Mango it is'));
    productsList.add(Product(
        image: '',
        productName: 'Papaya',
        quantity: 0,
        productDesc: 'Papaya it is'));
    productsList.add(Product(
        image: '',
        productName: 'Muskmelon',
        quantity: 0,
        productDesc: 'Muskmelon it is'));
  }
}
