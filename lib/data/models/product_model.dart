import 'dart:convert';

Product productFromJson(String str) => Product.fromJson(json.decode(str));

String productToJson(Product data) => json.encode(data.toJson());

class Product {
  String? productName;
  String? productDesc;
  int? quantity;
  String? image;

  Product({
    this.productName,
    this.productDesc,
    this.quantity,
    this.image,
  });

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        productName: json["productName"],
        productDesc: json["productDesc"],
        quantity: json["quantity"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "productName": productName,
        "productDesc": productDesc,
        "quantity": quantity,
        "image": image,
      };
}
